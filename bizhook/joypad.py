from typing import Union

from .base import Base
from .exceptions import InvalidRequest


class JoyPad(Base):
    """Client for sending joypad commands to Bizhawk"""

    def __init__(self, controller, address: str = '127.0.0.1', port: int = 16154):
        super(JoyPad, self).__init__(address, port)
        self.controller = controller

    def request(self, query: str):
        """Send a request to the Bizhawk Lua Emu hook

        Pattern:  HOOK/CMD/PARAM

        HOOK  - Module in the hook.lua to use
        FUN   - The command for the module to interpret
                set or get
        PARAM - The parameters for the command to use
                Further information in the PARAM is detailed in FUN's python function in this class
        """
        code, message = self._request(query)

        if code == 0:
            return True

        if code == 1:
            return eval(message.replace('=', '":')
                        .replace(',', ',"')
                        .replace('{', '{"')
                        .replace('"}', '}'))

        raise InvalidRequest(code, message)

    def _format_query(self, function: str, command: str):
        """Format all request parameters into a valid query for a request"""

        return f'joy/{function}/{command}'

    def set(self, inputs: Union[str, list]):
        """Send a request to the Bizhawk Lua Emu hook to set the inputs for the next frame
        FUN   - set
        PARAM - String or list of strings of inputs to push for the next frame in emuhawk, "P1 A"
        """
        if type(inputs) is list:
            inputs = [' '.join([self.controller, inputs]) for x in inputs]
            inputs = "/".join(inputs)
        else:
            inputs = ' '.join([self.controller, inputs])
        return self.request(self._format_query('set', inputs))

    def get(self, with_movie: bool = False):
        """Send a request to the Bizhawk Lua Emu hook to get the inputs currently pressed
        FUN   - get
        PARAM - with_movie/controller

        controller = the controller to get the inputs for, 1
        with_movie = return all pressed keys for movie and otherwise
        """
        return self.request(self._format_query('get',
                                               f'{with_movie}/{self.controller.replace("P", "")}'))
