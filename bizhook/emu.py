from .base import Base
from .exceptions import InvalidRequest


class Emu(Base):
    """Client for sending emulator commands to Bizhawk"""

    def __init__(self, address: str = '127.0.0.1', port: int = 16154):
        super(Emu, self).__init__(address, port)

    def request(self, query: str):
        """Send a request to the Bizhawk Lua Emu hook

        Pattern:  HOOK/FUN/PARAM

        HOOK  - Module in the hook.lua to use
        FUN   - The command for the module to interpret
                autostep or step
        PARAM - The parameters for the command to use
                Further information in the PARAM is detailed in FUN's python function in this class
        """
        code, message = self._request(query)

        if code == 0:
            return int(message)

        if code == 1:
            return message == 'true'

        raise InvalidRequest(code, message)

    def _format_query(self, function: str, command: str):
        """Format all request parameters into a valid query for a request"""

        return f'emu/{function}/{command}'

    def auto_step(self, mode: str):
        """Disables autostep of the emulator
        Note this causes the emulator to freeze

        :param mode: true or false
        :type mode: str"""
        return self.request(self._format_query('autostep', mode))

    def step(self, steps: str):
        """Step the emulator a number of frames"""
        return self.request(self._format_query('step', steps))
