from typing import Union

from .base import Base
from .exceptions import InvalidRequest


class Memory(Base):
    """Client for reading from and writing to Bizhawk memory"""

    def __init__(self, domain: str, address: str = '127.0.0.1', port: int = 16154):
        super(Memory, self).__init__(address, port)
        self.domain = domain

    def request(self, query: Union[bytes, str]):
        """Send a request to the Bizhawk Lua memory hook


        Pattern:  HOOK/DOMN/ADDR/TSLE/VLUE
        HOOK  - Module in the hook.lua to use
        DOMN  - Memory domain
        ADDR  - Address
        TSLE--.-------------------.
            Type:               Signage:
            | * [b]yte            * [u]nsigned
            | * [i]nteger         * [s]igned
            | * [f]loat
            |
            .-------------------.
            Length              Endianness
                * [1] byte          * [l]ittle endian
                * [2] bytes         * [b]ig endian
                * [3] bytes
                * [4] bytes

        VLUE  - Integer or float, ex. 12 or -1.2


        # EXAMPLES

        VRAM/11423051/iu4b/23
            Write (23) to (11423051) in (VRAM)
            an [i]nteger, [u]nsigned,
            [4] bytes long and [b]ig endian

        WRAM/21512962/fs2l/
            Read from (21512962) in (WRAM)
            a [f]loat, [s]igned,
            [2] bytes long and [l]ittle endian
        """

        # Socket requires a byte string to send
        code, message = self._request(query)

        # Successfully wrote to memory
        if code == 0:
            return True

        # Successfully read byte
        if code == 1:
            return int(message).to_bytes(1, 'big')

        # Successfully read integer
        if code == 2:
            return int(message)

        # Successfully read float
        if code == 3:
            return float(message)

        raise InvalidRequest(code, message)

    def _format_tsle(self, type_: type, signed: bool, length: int, endianness: str):
        """Format the type, signage, length, and endianness for a request"""

        if type_ not in (bytes, int, float):
            raise ValueError('Type must be bytes, int or float')

        if length not in (1, 2, 3, 4):
            raise ValueError('Length must be 1, 2, 3 or 4 bytes')

        if endianness not in ('little', 'big'):
            raise ValueError('Endianness must be little or big')

        return ''.join([
            type_.__name__[0],
            'us'[signed],
            str(length),
            endianness[0]
        ])

    def _format_query(self, address: int, type_: type = bytes, signed: bool = False,
                      length: int = 1, endianness: str = 'big', value: Union[int, float] = None):
        """Format all request parameters into a valid query for a request"""

        tsle = self._format_tsle(type_, signed, length, endianness)
        return f'mem/{self.domain}/{address}/{tsle}/{"" if value is None else value}'

    def read_byte(self, address: int):
        """Read byte from memory"""
        return self.request(self._format_query(
            address=address,
            type_=bytes,
            value=None
        ))

    def write_byte(self, address: int, value: int):
        """Write byte from memory"""
        return self.request(self._format_query(
            address=address,
            type_=bytes,
            value=value
        ))

    def read_int(self, address: int, signed: bool = False, length: int = 1, endianness: str = 'big'):
        """Read integer from memory"""
        return self.request(self._format_query(
            address=address,
            type_=int,
            signed=signed,
            length=length,
            endianness=endianness,
            value=None
        ))

    def write_int(self, address: int, value: int, signed: bool = False, length: int = 1, endianness: str = 'big'):
        """Write integer from memory"""
        return self.request(self._format_query(
            address=address,
            type_=int,
            signed=signed,
            length=length,
            endianness=endianness,
            value=value
        ))

    def read_float(self, address: int, endianness: str = 'big'):
        """Read float from memory"""
        return self.request(self._format_query(
            address=address,
            type_=float,
            endianness=endianness,
            value=None
        ))

    def write_float(self, address: int, value: int, endianness: str = 'big'):
        """Write float from memory"""
        return self.request(self._format_query(
            address=address,
            type_=float,
            endianness=endianness,
            value=value
        ))
