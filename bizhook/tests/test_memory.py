import subprocess

import pytest

from bizhook import Memory
import time

# Setup the ROM to test
# todo get this working
# rom_url = 'https://superbossgaiden.superfamicom.org/download.php'
# rom = requests.get(rom_url, allow_redirects=True)
# open(Path(__file__).parent / 'superbossgaiden.smc', 'wb').write(rom.content)


# Setup the memory object
@pytest.fixture()
def cartrom():
    return Memory('CARTROM')


@pytest.fixture()
def wram():
    return Memory('WRAM')


@pytest.fixture()
def bizhawk():
    # Setup the bizhawk exe
    # todo build docker image for this to run in gitlab
    bizhawk = subprocess.Popen(["build/BizHawk-2.6.3-win-x64/EmuHawk.exe",
                                "--lua=lua_components/hook.lua",
                                "build/super_boss_gaiden/Super Boss Gaiden (J) (V1.2).sfc"])

    time.sleep(10)
    yield bizhawk

    bizhawk.kill()


def test_read_cartrom(bizhawk, cartrom):
    assert int.from_bytes(cartrom.read_byte(0xF000), 'big') == 108
    assert cartrom.read_int(0xF000) == 108
    assert cartrom.read_int(0xF000, endianness='little') == 108
    assert cartrom.read_int(0xF000, length=4) == 1812308778
    assert cartrom.read_int(0xF000, length=4, signed=True) == 1812308778
    assert cartrom.read_float(0xF000) == 6.4623061803482e+26
    assert cartrom.read_float(0xF000, endianness='little') == 2.8958378663763e-13


def test_read_wram(bizhawk, wram):
    assert int.from_bytes(wram.read_byte(0x2000), 'big') == 85
    assert wram.read_int(0x1000) == 85
