import subprocess

import pytest

from bizhook import JoyPad
import time

# Setup the ROM to test
# todo get this working
# rom_url = 'https://superbossgaiden.superfamicom.org/download.php'
# rom = requests.get(rom_url, allow_redirects=True)
# open(Path(__file__).parent / 'superbossgaiden.smc', 'wb').write(rom.content)


# Setup the memory object
@pytest.fixture()
def joypad():
    return JoyPad('P1')


@pytest.fixture()
def bizhawk():
    # Setup the bizhawk exe
    # todo build docker image for this to run in gitlab
    bizhawk = subprocess.Popen(["build/BizHawk-2.6.3-win-x64/EmuHawk.exe",
                                "--lua=lua_components/hook.lua",
                                "build/super_boss_gaiden/Super Boss Gaiden (J) (V1.2).sfc"])

    time.sleep(10)
    yield bizhawk

    bizhawk.kill()


def test_joypad(bizhawk, joypad):
    assert joypad.set('Start')
    assert joypad.get(with_movie=True)
    time.sleep(10)
