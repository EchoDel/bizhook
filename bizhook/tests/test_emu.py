import subprocess

import pytest

from bizhook import Emu
import time

# Setup the ROM to test
# todo get this working
# rom_url = 'https://superbossgaiden.superfamicom.org/download.php'
# rom = requests.get(rom_url, allow_redirects=True)
# open(Path(__file__).parent / 'superbossgaiden.smc', 'wb').write(rom.content)


# Setup the memory object
@pytest.fixture()
def emu():
    return Emu()


@pytest.fixture()
def bizhawk():
    # Setup the bizhawk exe
    # todo build docker image for this to run in gitlab
    bizhawk = subprocess.Popen(["build/BizHawk-2.6.3-win-x64/EmuHawk.exe",
                                "--lua=lua_components/hook.lua",
                                "build/super_boss_gaiden/Super Boss Gaiden (J) (V1.2).sfc"])

    time.sleep(10)
    yield bizhawk

    bizhawk.kill()


def test_read_cartrom(bizhawk, emu):
    assert emu.auto_step("true")
    assert not emu.auto_step("false")
    assert emu.step(10) == 10
