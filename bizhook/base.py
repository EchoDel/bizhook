from socket import socket as Socket
from socket import create_connection

from typing import Union

from .exceptions import InvalidResponse


class Base:
    """Base class for interacting with the bizhook server"""

    def __init__(self, address: str = '127.0.0.1', port: int = 16154):
        self.address = address
        self.port = port

    def _request(self, query: Union[bytes, str]):
        """Send a request to the Bizhawk Lua hook"""

        # Socket requires a byte string to send
        if type(query) is not bytes:
            query = query.encode()

        with create_connection((self.address, self.port)) as socket:
            # Send request and expect response
            socket.sendall(query)
            response = self._receive(socket)

        try:
            # Extract response code and message
            code, _, message = response.decode('UTF-8').partition('_')
            code = int(code)

        except ValueError:
            raise InvalidResponse('Response could not be divided into code and message')

        return code, message

    def _receive(self, socket: Socket, n: int = 128):
        """Receive data until end of stream"""

        # Receive data in packets
        # and concatenate them at the end

        buffer = []

        while True:
            data = socket.recv(n)

            if not data:
                break

            buffer.append(data)

        return b''.join(buffer)
