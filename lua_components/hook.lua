---------------------------------------------------------
-- The Lua side of things would not have been possible --
-- if it weren't for the help of one lovely individual --
--                                                     --
-- Tumn                                                --
--   * Matrix: @autumn:raincloud.dev                   --
--   * GitHub: github.com/rosemash                     --
---------------------------------------------------------

address, port = "127.0.0.1", 16154

---------------------------------------------------------
-- # HOW TO USE                                        --
--                                                     --
-- Requests can be made by connecting to the socket    --
-- at the specified address and port and sending       --
-- a message following the pattern below.              --
--                                                     --
--    OBS! Every request requires a new connection.    --
--                                                     --
--                                                     --
-- Pattern:  DOMN/ADDR/TSLE/VLUE                       --
--                                                     --
-- DOMN  - Memory domain                               --
-- ADDR  - Address                                     --
-- TSLE--.-------------------.                         --
--       Type:               Signage:                  --
--       | * [b]yte            * [u]nsigned            --
--       | * [i]nteger         * [s]igned              --
--       | * [f]loat                                   --
--       |                                             --
--       .-------------------.                         --
--       Length              Endianness                --
--         * [1] byte          * [l]ittle endian       --
--         * [2] bytes         * [b]ig endian          --
--         * [3] bytes                                 --
--         * [4] bytes                                 --
--                                                     --
--  VLUE  - Integer or float, ex. 12 or -1.2           --
--                                                     --
--                                                     --
--  # EXAMPLES                                         --
--                                                     --
--   VRAM/11423051/iu4b/23                             --
--     Write (23) to (11423051) in (VRAM)              --
--       an [i]nteger, [u]nsigned,                     --
--       [4] bytes long and [b]ig endian               --
--                                                     --
--   WRAM/21512962/fs2l/                               --
--     Read from (21512962) in (WRAM)                  --
--       a [f]loat, [s]igned,                          --
--       [2] bytes long and [l]ittle endian            --
--                                                     --
---------------------------------------------------------


-- Include necessary socket modules using a hack

local version = _VERSION:match("%d+%.%d+")
do_step = true
frames_to_advance = 0

package.path = 'lua_modules/share/lua/'
			.. version
			.. '/?.lua;lua_modules/share/lua/'
			.. version
			.. '/?/init.lua;'
			.. package.path

package.cpath = 'lua_modules/lib/lua/'
			.. version
			.. '/?.so;'
			.. package.cpath


-- Import modules and set up socket connection

socket = require("socket")
copas = require("copas")

-- Import the modules to be used in the request
biz_hook_memory = require("memory")
biz_hook_joypad = require("joypad")
biz_hook_emu = require("emu")


server = socket.bind(address, port)


local function clientHandler(client)
	-- Reads data until client is disconnected
	-- and processes the data with handleRequest

	local data = ""

	while true do
		-- Read 1 byte at a time
		chunk, errmsg = client:receive(1)
		
		-- Quit reading if an error has occured
		-- or no data was received
		if not chunk then
			if errmsg == 'timeout' then
				break
			end
			
			print(('Socket error: %s'):format(errmsg))
			return
		end

		-- Append new byte to data
		data = data .. chunk
	end

	if not data then return end

    -- Split the request into the "module" and "request"
    -- e.g. mem      VRAM/11423051/iu4b/23
    handler = string.sub(data, 1, 3)
    request = string.sub(data, 5, #data)
	-- Handle the request
	-- and formulate a response
	-- Each handle is a separate lua module based on the Emuhawk module
	if handler == 'mem' then
	    response = biz_hook_memory.handleRequest(request)
	elseif handler == 'joy' then
	    response = biz_hook_joypad.handleRequest(request)
	elseif handler == 'emu' then
	    response = biz_hook_emu.handleRequest(request)
	end

	if not response then return end

	-- Make sure response is string
	-- and send back response to client
	client:send(tostring(response))
end


copas.addserver(server, clientHandler)


-- Open up socket with a clear sign
while emu.framecount() < 2 do
	gui.text(20, 20, '.Opening socket at ' .. address .. ':' .. port)
	emu.frameadvance()
end


while true do
	-- Communicate with client
	local handled, errmsg = copas.step(0)
	if handled == nil then
		print(('Socket error: %s'):format(errmsg))
	end
	
	-- Advance the game by a frame
	if do_step then
		emu.frameadvance()
    elseif frames_to_advance > 0 then
	    emu.frameadvance()
	    frames_to_advance = frames_to_advance - 1
	end
end
