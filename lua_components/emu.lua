local helpers = require("helpers")

local biz_hook_emu = {}

-- Response codes
local rcodes = {
	STEP = 0;  -- Successfully changed the step mode
	AUTOSTEP = 1;  -- Successfully changed the step mode
	ERROR   = 4;  -- Generic error
}

local function auto_step_emu(data)
    local step_mode = data == 'true'
    do_step = step_mode
    return helpers.format_response(
        rcodes.AUTOSTEP,
        step_mode
        )
end

local function step_emu(data)
    frames_to_advance = tonumber(data)

    return helpers.format_response(
        rcodes.STEP,
        tonumber(data)
        )
end

function biz_hook_emu.handleRequest(data)
	-- Handle incoming requests for the joypad with
	-- with the BizHawk emulator

    mode, command = string.match(data, "(.-)%/(.*)")

    if mode == 'step' then
        return step_emu(command)
    elseif mode == 'autostep' then
        return auto_step_emu(command)
    end
end


-- todo add basic joypad inputs
-- enable single frame advance with syncing of inputs
return biz_hook_emu
