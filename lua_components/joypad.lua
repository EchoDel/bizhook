local helpers = require("helpers")

local biz_hook_joypad = {}

-- Response codes
local rcodes = {
	SET = 0;  -- Successfully set the key positions
	GET = 1;  -- Successfully got the key positions
	ERROR   = 4;  -- Generic error
}

local function set_keys(data)
    -- Handle the set keys command

    local keys = {}
    local remaining = data

    while true do
        if string.match(data, '/') then
            remaining, command = string.match(remaining, "(.*)%/(.*)")

            keys[command] = true
        else
            keys[remaining] = true
            break
        end
    end

    frames_to_advance = 1

    return helpers.format_response(
        rcodes.SET,
        joypad.set(keys)
        )
end

local function get_keys(data)
    withmovie, controller = string.match(data, "(.*)%/(.*)")
    controller = tonumber(controller)
    frames_to_advance = 1

    if withmovie == 'true' then
        inputs = joypad.getwithmovie(controller)

        return helpers.format_response(
            rcodes.GET,
            helpers.table_print(inputs)
        )
    else
        inputs = joypad.get(controller)

        return helpers.format_response(
            rcodes.GET,
            helpers.table_print(inputs)
        )
    end
end

function biz_hook_joypad.handleRequest(data)
	-- Handle incoming requests for the joypad with
	-- with the BizHawk emulator

    mode, command = string.match(data, "(.-)%/(.*)")

    if mode == 'set' then
        return set_keys(command)
    elseif mode == 'get' then
        return get_keys(command)
    end
end


-- todo add basic joypad inputs
-- enable single frame advance with syncing of inputs
return biz_hook_joypad
