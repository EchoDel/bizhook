local helpers = require("helpers")

local biz_hook_memory = {}

-- Response codes
local rcodes = {
	WRITTEN = 0;  -- Successfully wrote to memory
	BYTE    = 1;  -- Successfully read byte
	INTEGER = 2;  -- Successfully read integer
	FLOAT   = 3;  -- Successfully read float
	ERROR   = 4;  -- Generic error
}


function biz_hook_memory.handleRequest(data)
	-- Handle incoming requests for reading from
	-- and writing to memory with the BizHawk emulator

	domain, address, type, signage, length, endianness, value
        = data:match('^([%w%s]*)%/(%d+)%/([bif])([us])([1234])([lb])%/(-?%d*%.?%d*)$')

	-- Use default domain if none is provided
	if domain == "" then
		domain = nil
	end

	-- Convert address to integer
	address = tonumber(address)


	-- local function format_response(code, message)
	-- 	-- Format response code and message into a valid response
	-- 	return tostring(code) .. '_' .. tostring(message)
	-- end


	-- [ READ ]
	if value == "" then

		-- [ BYTE ]
		if type == 'b' then
			return helpers.format_response(
				rcodes.BYTE,
				memory.readbyte(address, domain)
			)
		end


		-- [ INTEGER ]
		if type == 'i' then

			-- [ UNSIGNED ]
			if signage == 'u' then

				-- [ 1 BYTE ]
				if length == '1' then
					return helpers.format_response(
						rcodes.INTEGER,
						memory.read_u8(address, domain)
					)
				end

				-- [ LITTLE ENDIAN ]
				if endianness == 'l' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_u16_le(address, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_u24_le(address, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_u32_le(address, domain)
						)
					end
				end

				-- [ BIG ENDIAN ]
				if endianness == 'b' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_u16_be(address, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_u24_be(address, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_u32_be(address, domain)
						)
					end
				end
			end

			-- [ SIGNED ]
			if signage == 's' then

				-- [ 1 BYTE ]
				if length == '1' then
					return helpers.format_response(
						rcodes.INTEGER,
						memory.read_s8(address, domain)
					)
				end

				-- [ LITTLE ENDIAN ]
				if endianness == 'l' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_s16_le(address, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_s24_le(address, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_s32_le(address, domain)
						)

					end
				end

				-- [ BIG ENDIAN ]
				if endianness == 'b' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_s16_be(address, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_s24_be(address, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.INTEGER,
							memory.read_s32_be(address, domain)
						)

					end
				end
			end
		end

		-- [ FLOAT ]
		if type == 'f' then

			-- Whether the value is big endian or not
			bigendian = endianness == 'b'

			return helpers.format_response(
				rcodes.FLOAT,
				memory.readfloat(address, bigendian, domain)
			)
		end

	-- [ WRITE ]
	else

		-- Convert value to number
		value = tonumber(value)


		-- [ BYTE ]
		if type == 'b' then
			return helpers.format_response(
				rcodes.WRITTEN,
				memory.writebyte(address, value, domain)
			)
		end


		-- [ INTEGER ]
		if type == 'i' then

			-- [ UNSIGNED ]
			if signage == 'u' then

				-- [ 1 BYTE ]
				if length == '1' then
					return helpers.format_response(
						rcodes.WRITTEN,
						memory.write_u8(address, value, domain)
					)
				end

				-- [ LITTLE ENDIAN ]
				if endianness == 'l' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_u16_le(address, value, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_u24_le(address, value, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_u32_le(address, value, domain)
						)
					end
				end

				-- [ BIG ENDIAN ]
				if endianness == 'b' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_u16_be(address, value, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_u24_be(address, value, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_u32_be(address, value, domain)
						)
					end
				end
			end

			-- [ SIGNED ]
			if signage == 's' then

				-- [ 1 BYTE ]
				if length == '1' then
					return helpers.format_response(
						rcodes.WRITTEN,
						memory.write_s8(address, value, domain)
					)
				end

				-- [ LITTLE ENDIAN ]
				if endianness == 'l' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_s16_le(address, value, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_s24_le(address, value, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_s32_le(address, value, domain)
						)
					end
				end

				-- [ BIG ENDIAN ]
				if endianness == 'b' then

					-- [ 2 BYTE ]
					if length == '2' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_s16_be(address, value, domain)
						)

					-- [ 3 BYTE ]
					elseif length == '3' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_s24_be(address, value, domain)
						)

					-- [ 4 BYTE ]
					elseif length == '4' then
						return helpers.format_response(
							rcodes.WRITTEN,
							memory.write_s32_be(address, value, domain)
						)
					end
				end
			end
		end

		-- [ FLOAT ]
		if type == 'f' then

			-- Whether the value is big endian or not
			bigendian = endianness == 'b'

			return helpers.format_response(
				rcodes.WRITTEN,
				memory.writefloat(address, value, bigendian, domain)
			)
		end
	end


	-- If nothing is matched,
	-- let the client know that something's gone wrong
	return helpers.format_response(rcodes.ERROR, 'INVALID_REQUEST')
end

return biz_hook_memory
