helpers = {}

function helpers.format_response(code, message)
	-- Format response code and message into a valid response
	return tostring(code) .. '_' .. tostring(message)
end

function helpers.table_print (tt, indent, done)
    -- Formats a lua table as a string
    done = done or {}
    indent = indent or 0
    if type(tt) == "table" then
        local sb = {}
        table.insert(sb, '{')
        for key, value in pairs (tt) do
            table.insert(sb, string.rep(" ", indent)) -- indent it
            if type (value) == "table" and not done [value] then
                done [value] = true
                table.insert(sb, key .. " = {\n");
                table.insert(sb, table_print(value, indent + 2, done))
                table.insert(sb, string.rep(" ", indent)) -- indent it
                table.insert(sb, "}\n");
            elseif "number" == type(key) then
                table.insert(sb, string.format("\"%s\"\n", tostring(value)))
            else
                table.insert(sb, string.format(
                "%s=\"%s\",", tostring(key), tostring(value)))
        end
    end
    table.insert(sb, '}')
        return table.concat(sb)
    else
        return tt .. "\n"
    end
end


return helpers
