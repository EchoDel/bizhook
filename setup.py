from setuptools import setup

from pathlib import Path
this_directory = Path(__file__).parent
long_description = (this_directory / "README.md").read_text()

setup(
    name='bizhook',
    version='1.0.0',
    description='Communicate with the Bizhawk emulator via a Lua socket',
    author='Maximillian Strand',
    author_email='maximillian.strand@protonmail.com',
    url='https://gitlab.com/thedisruptproject/bizhook',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=['bizhook'],
    install_requires=[],
    setup_requires=['pytest-runner', 'flake8<4'],
    tests_require=['pytest', 'pytest-flake8', 'requests'],
    package_data={'bizhook': ['lua_components.zip']},
    license='GNU General Public License v3.0',
    platforms='any',
)
